#ifndef ITERATIVE_TIMER_RUNNER_H
#define ITERATIVE_TIMER_RUNNER_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

typedef struct Timer {
    SDL_Texture *screen;
    int duration;
    struct Timer *next;
} Timer;

void run( /* NOTE: corrupts the durations of the timer. */
        SDL_Renderer *renderer,
        Timer *t, 
        TTF_Font *font,
        SDL_Color colour,
        int font_x,
        int font_y,
        char *format);
void free_timer(Timer *t); /* recursive */
void flip_timer(Timer **t); 

#endif /* ITERATIVE_TIMER_RUNNER_H */
