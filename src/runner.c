#include "runner.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void run(
        SDL_Renderer *renderer,
        Timer *t,
        TTF_Font *font,
        SDL_Color colour,
        int font_x,
        int font_y,
        char *format)
{
    int done = 0;
    int paused = 0;
    int ticks = SDL_GetTicks();
    SDL_Event e;
    char msg[256];
    char prnt[256];

    while (!done) {
        int tmp;
        while (SDL_PollEvent(&e)) {
            switch (e.type) {
                case SDL_KEYDOWN:
                    if (e.key.keysym.sym == ' ') paused = !paused;
                    if (e.key.keysym.sym == SDLK_ESCAPE) done = 1;
                    break;
                case SDL_QUIT:
                    done = 1;
                    break;
            }
        }

        if (paused) {
            ticks = SDL_GetTicks();
            continue;
        }

        tmp = SDL_GetTicks();
        t->duration -= (tmp - ticks);

        ticks = tmp;

        if (t->duration < 0) {
            tmp = t->duration; /* how many negative ms were wasted on last scr*/
            t = t->next;
            
            if (!t) return;
            t->duration += tmp; /* subtract the extra seconds. */
        }

        /* draw screen */
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer,t->screen,NULL,NULL);

        /* draw time remaining, if possible */
        if (font) {
            SDL_Surface *sur;
            SDL_Texture *tex;
            SDL_Rect r;
            int hours,minutes,seconds,ms;
            struct tm tm_struct;

            ms = t->duration;
            seconds = ms / 1000;
            ms %= 1000;
            minutes = seconds / 60;
            seconds %= 60;
            hours = minutes / 60;
            minutes %= 60;

            tm_struct.tm_sec = seconds;
            tm_struct.tm_min = minutes;
            tm_struct.tm_hour = hours % 24;
            tm_struct.tm_mday = hours / 24;

            strftime(msg,256,format,&tm_struct);
            sprintf(prnt,msg,ms);

            sur = TTF_RenderText_Solid(font,prnt,colour);

            if (!sur) {
                printf("Error: could not render text \"%s\" because...\n %s\n",
                        msg, TTF_GetError());
                return;
            }

            tex = SDL_CreateTextureFromSurface(renderer,sur);

            r.x = font_x;
            r.y = font_y;
            r.w = sur->w;
            r.h = sur->h;

            SDL_RenderCopy(renderer,tex,NULL,&r);

            SDL_DestroyTexture(tex);
            SDL_FreeSurface(sur);
        }
        SDL_RenderPresent(renderer);
    }
}

void free_timer(Timer *t)
{
    Timer *tmp;
    while (t) {
        tmp = t;
        t = t->next;
        SDL_DestroyTexture(tmp->screen);
        free(tmp);
    }
}

void flip_timer(Timer **t)
{
    Timer *this = *t;
    Timer *ret = NULL;
    
    while (this) {
        Timer *new = malloc(sizeof(Timer));
        new->screen = this->screen;
        new->duration = this->duration;
        new->next = ret;
        ret = new;

        /* Freeing old timer here. */
        new = this;
        this = this->next;
        free(new);
    }

    *t = ret;
}
