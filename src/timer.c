#include "timer.h"
#include "runner.h"

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#define MAX_FNAME_LENGTH 2048

void run_timer(
        char *fname,
        SDL_Renderer *renderer,
        TTF_Font *font,
        SDL_Color colour,
        int font_x,
        int font_y,
        char *format)
{
    int i;
    char *buffer;
    Timer *t = NULL; 
    FILE *file = fopen(fname,"r");

    if (!file) {
        printf("Error: File %s could not be opened!\n",fname);
        return;
    }

    
    buffer = malloc(sizeof(char) * MAX_FNAME_LENGTH);
    i = 0;
    while (!feof(file)) {
        SDL_Surface *sur;
        char tmp;
        Timer *new;
        do {
            tmp = fgetc(file);
            buffer[i++] = tmp;
            if (i == MAX_FNAME_LENGTH - 1) break;
            if (feof(file)) {
                /*premature end of file, leave loop, do not add new */
                goto skip_add_new;
            }
        } while (tmp != '\n');

        buffer[i--] = 0; /* Null-terminate the string. */

        /* Skip to the number at the end. This technically allows for
         * non-numeric comments! */
        while (!isdigit(buffer[i])) {
            i--;
        }
        buffer[i+1] = 0;

        /* Get the index of the first digit of the number. */
        while (isdigit(buffer[i])) {
            i--;
        }

        new = malloc(sizeof(Timer));
        new->duration = atoi(buffer+i+1);
        buffer[i] = 0; /* null-terminate the file name */
        sur = IMG_Load(buffer);
        
        if (!sur) {
            printf("Error: Could not open %s as image: %s\n",buffer,IMG_GetError());
            free(buffer);
            free_timer(t);
            return;
        }

        new->screen = SDL_CreateTextureFromSurface(renderer,sur);
        new->next = t;
        t = new;
        i = 0;
        SDL_FreeSurface(sur);
    }
skip_add_new:

    free(buffer);

    /* timer is backwards since we use a stack. */
    flip_timer(&t);

    run(renderer,t,font,colour,font_x,font_y,format);

    free_timer(t);
}
            
