#ifndef ITERATIVE_TIMER_H
#define ITERATIVE_TIMER_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

void run_timer(
        char *fname, 
        SDL_Renderer *renderer, 
        TTF_Font *font, 
        SDL_Color colour,
        int font_x,
        int font_y,
        char *format);

#endif /* ITERATIVE_TIMER_H */
