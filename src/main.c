#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>

#include "timer.h"

SDL_Window *window;
SDL_Renderer *renderer;

int help(void)
{
    return printf(
"it [options] infile - Runs an iterative timer of the given screens.\n"
"\n"
"\tinfile is a file containing pairs of filenames and numbers, one pair per\n"
"\tline. The numbers indicate the duration for which to show the given image,\n"
"\tin ms. The images will be shown in the order they appear in the file.\n"
"\n"
"\tAll options are followed by the value to which to set the given option. \n"
"\tThey are as follows:\n"
"\n"
"\t\t-t\tSpecifies the title of the window (default: Timer).\n"
"\t\t-w\tSpecifies the width of the window (default: 640px).\n"
"\t\t-h\tSpecifies the height of the window (default: 480px).\n"
"\t\t-f\tSpecifies the font to render the remaining time with (default: -).\n"
"\t\t-s\tSpecifies the size of the above font (default: 14).\n"
"\t\t-x\tSpecifies the x-offset at which to render the time (default: 0).\n"
"\t\t-y\tSpecifies the y-offset at which to render the time (default: 0).\n"
"\t\t-c\tSpecifies the colour with which to render the time (default: 0xFF000000).\n"
"\t\t-r\tSpecifies the format to use when rendering the time...\n"
"\t\t\tThe provided string will first be passed through strftime\n"
"\t\t\tthen through sprintf with the additional milliseconds as argument.\n"
"\t\t\tTake note that the buffer for the message only holds 256 characters.\n"
"\t\t\t(default: \"%%H:%%M:%%S.%%%%03d\")\n"
"\n"
"\tNote: if no input file is specified, standard input will be used instead.\n"
    );
}

int init_sdl(void) 
{
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        printf("Error: SDL failed to init: %s\n",SDL_GetError());
        return 1;
    }
    
    if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG) {
        printf("Error: IMG failed to init: %s\n",IMG_GetError());
        return 2;
    }

    if (TTF_Init() == -1) {
        printf("Error: TTF failed to init: %s\n",TTF_GetError());
        return 3;
    }

    return 0;
}

int close_sdl(void) 
{
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
    return 0;
}

int main(int argc, char *argv[])
{
    int i;
    char *title = "Timer";
    int width=640;
    int height=480;
    char *font = NULL;
    int font_size = 14;
    int font_x = 0;
    int font_y = 0;
    unsigned int font_col = 0x0;
    char *file = NULL;
    char *format = "%H:%M:%S.%%03d";
    TTF_Font *ttf = NULL;
    SDL_Color col;

    if (argc == 1) return help();

    for (i = 1; i < argc; ++i) {
        if (argv[i][0] == '-') {
            switch (argv[i][1]) {
                case 't':
                    if (i == argc-1) {
                        printf("Error: -t not followed by title.\n");
                        return 1;
                    }

                    title = argv[(i++)+1];
                    break;
                case 'w':
                    if (i == argc-1) {
                        printf("Error: -w not followed by width.\n");
                        return 1;
                    }

                    width = atoi(argv[(i++)+1]);
                    break;
                case 'h':
                    if (i == argc-1) {
                        printf("Error: -h not followed by height.\n");
                        return 1;
                    } 

                    height = atoi(argv[(i++)+1]);
                    break;
                case 'f':
                    if (i == argc-1) {
                        printf("Error: -f not followed by font name.\n");
                        return 1;
                    } 

                    font = argv[(i++)+1];
                    break;
                case 's':
                    if (i == argc-1) {
                        printf("Error: -s not followed by font size.\n");
                        return 1;
                    }
                    
                    font_size = atoi(argv[(i++)+1]);
                    break;
                case 'x':
                    if (i == argc-1) {
                        printf("Error: -x not followed by font x-offset.\n");
                        return 1;
                    }

                    font_x = atoi(argv[(i++)+1]);
                    break;
                case 'y':
                    if (i == argc-1) {
                        printf("Error: -y not followed by font y-offset.\n");
                        return 1;
                    }

                    font_y = atoi(argv[(i++)+1]);
                    break;
                case 'c':
                    if (i == argc-1) {
                        printf("Error: -c not followed by font colour.\n");
                        return 1;
                    }

                    font_col = strtol(argv[(i++)+1],NULL,0);
                    break;
                case 'r':
                    if (i == argc-1) { 
                        printf("Error: -r not followed by format.\n");
                        return 1;
                    }

                    format = argv[(i++)+1];
                    break;
                default:
                    printf("Error: Unknown option: %s", argv[i]);
                    return 1;
            }
        } else {
            if (file != NULL) {
                printf("Error: Multiple files provided.");
                return 1;
            }
            file = argv[i];
        }
    }
            
    if (init_sdl()) return 1;
    
    window = SDL_CreateWindow(
            title,
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            width,
            height,
            SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

    if (!window) {
        printf("Error: Window failed to init: %s\n",SDL_GetError());
        return 1;
    }

    renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
    
    if (!renderer) {
        printf("Error: Renderer failed to init: %s\n",SDL_GetError());
        SDL_DestroyWindow(window);
        return 1;
    }

    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    if (font) {
        ttf = TTF_OpenFont(font,font_size);
        if (!ttf) {
            printf("Error: Font failed to load: %s\n",TTF_GetError());
            return 1;
        }
        col.a = (font_col >> 24) & 0xFF;
        col.r = (font_col >> 16) & 0xFF;
        col.g = (font_col >> 8) & 0xFF;
        col.b = (font_col) & 0xFF;
    }

    run_timer(file,renderer,ttf,col,font_x,font_y,format);

    if (ttf) TTF_CloseFont(ttf);
    if (close_sdl()) return 1;
    return 0;
}

